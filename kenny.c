#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>

int health;

void sigHandler(int signal) {
    printf("C'est injuste!\n");
    health -= signal;
}

void invincible(int sig) {
    int delay = 10;



    while (delay --> 0) {
        printf("Je suis invincible");
        sleep(1);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Utilisation : %s <points_de_vie>\n", argv[0]);
        return 1;
    }

    health = atoi(argv[1]); // Initialiser les points de vie à partir de l'argument

    if (health <= 0) {
        fprintf(stderr, "Les points de vie doivent être supérieurs à zéro.\n");
        return 1;
    }

    printf("Kenny est en vie avec %d points de vie.\n", health);

    for (int t = 1; t <= 10; t++) {
        if (t == 10) {
            signal(t, invincible);
        }
        else {
            signal(t, sigHandler);
        }
    }

    while (health > 0) {
        printf("Points de vie : %d\n", health);
        printf("C'est trop injuste.\n");
        system("echo $!");
        sleep(3);
    }

    printf("Kenny est mort.\n");
    return 0;
}
